# Firework

проект Фейерверка



// Разработал: Вадим Ярмушов 
// ver:	0.15 от 30.12.2020


Фейерверк. Выполнен с применением WinApi и OpenGL в Visual Studio 2019
1. подключение библиотек: в зависимостях проекта добавить библиотеки: OpenGL32.lib GLu32.lib 
2. В Properties->Configuration Properties->Advanced->Charaster set выбран пункт : Use Unicode Charaster set
3. В Properties->Configuration Properties->Linker->System->SubSystem выбран пункт: Windows
                    
из дополнительных библиотек был скачан файл с сайта NVIDIA: wglext.h - для веритикальной синхронизации экрана bVSync

В каталоге с exe файлом должен быть графический файл: city03.tga, без него текстуры буду  отсутствовать - будет белый экран.

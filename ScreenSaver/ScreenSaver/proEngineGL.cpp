#include "proEngineGL.h"

#include "proImage.h"
#include "proInterfaceUI.h"
#include <TCHAR.h>

#include "proScreenSaver.h"



extern pro::screensaver::Firework * pFirework;



using namespace pro::types;
using namespace pro::engine;



pro::engine::EngineGL::EngineGL()
{
	fFrameTimer = 1.0f;
	nFrameCount = 0;
	nLastFPS = 0;
	fFrameTimer = 1.0f;
	fElapsedTime = 0.0f;

	bVSync = true;	// ������� ������������ �������������
}



// true -- VSync �������, false -- ��������
void pro::engine::EngineGL::set_vsync(bool enabled)
{
		
	
		PFNWGLSWAPINTERVALEXTPROC wglSwapInterval = NULL;

		wglSwapInterval = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");

		 if (wglSwapInterval)
		 {

			 if (enabled)
			 {
				 wglSwapInterval(1);
			 }
			 else
			 {
				 wglSwapInterval(0);
			 }
		 }

}



void EngineGL::DrawRoundPixel(float x, float y, COLOR_RGB color, int sizePixel)
{
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glPointSize(sizePixel);
	glBegin(GL_POINTS);
		//glColor3f(1, 0, 0);
		color.setColor();
		glVertex2f(x, y);
	glEnd();

}



void EngineGL::DrawSquarePixel(float x, float y, COLOR_RGB color, int sizePixel)
{
	
	glPushMatrix();
	glPointSize(sizePixel);

	glBegin(GL_POINTS);
		color.setColor();
		glVertex2f(x, y);
	glEnd();

	glPopMatrix();
}



void EngineGL::LoadTexture(const char* pFilename)
{

	using namespace pro::filesystem;

	
	
	img.loadtga(pFilename);
	int numChannel;
	GLenum  pixelFormat;
	GLint internalFormat;

	switch (img.bpp)
	{
	case 8:  {numChannel = 1; pixelFormat = GL_LUMINANCE;	internalFormat = GL_LUMINANCE;	break; }
	case 24: {numChannel = 3; pixelFormat = GL_RGB;			internalFormat = GL_RGB;		break; }
	case 32: {numChannel = 4; pixelFormat = GL_RGBA;		internalFormat = GL_RGBA;		break; }
	}

	int width = img.width;
	int height = img.height;
	


	const int num_texture = 1;


	glGenTextures(num_texture, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, pixelFormat, GL_UNSIGNED_BYTE, img.inddata);
	glBindTexture(GL_TEXTURE_2D, 0);	// ��������� �������� ��������	

	img.clear();				// ��������� ������ �� �����������

}




void pro::engine::EngineGL::UpdateTitleBar(float fElapsedTime)
{
	fFrameTimer += fElapsedTime;			// ������� ���������� ������� � ������ �������� ������
	nFrameCount++;							// ������� ������
	if (fFrameTimer >= 1.0f)				// ����� ������� ������� ��������� ������� � 1 ������� ������� � title ���������� ��������� ������ �� ��� �����
	{
		nLastFPS = nFrameCount;				// ������� ���������� ���� ��������� ������ �� ������ ������
		fFrameTimer -= 1.0f;				// �������� ������ �� 1 �������, �.�. ������� ��� (���������� fFrameTimer = 0.0f;	)
				
		std::wstring sTitle = _T("Firework - FPS: ") + std::to_wstring(nFrameCount) + _T("  �������: ") + std::to_wstring(info.infoShots) + _T("  ������: ") + std::to_wstring(info.particles);
		SetWindowText(hwnd, sTitle.c_str());
		nFrameCount = 0;
	}
}



// ============== ��� ��������� ������� ��������� ====================
// �������� ������� ���������� � ��������
// Xw -������� ���������� � ��������
// Xnd - ������������� ���������� �� -1 �� 1
// X -���������� x ������� ������ ���� ������
// width - ������ ���� � ��������
int EngineGL::getXw(float Xnd, int width , int X )
{
	return ((Xnd + 1.0) * ((float)width / 2)) + X;

}



// �������� ������� ���������� Yw � ��������
// Yw -������� ���������� � ��������
// Ynd - ������������� ���������� �� -1 �� 1
// Y -���������� x ������� ������ ���� ������
// height - ������ ���� � ��������
int EngineGL::getYw(float Ynd, int height, int Y)
{


	return (-1) * (((Ynd + 1.0) * ((float)height / 2)) - Y);

}



// �������� ������� ���������� Xnd � �������� [-1 �� 1]
// Xw -������� ���������� � ��������
// Xnd - ������������� ���������� �� -1 �� 1
// X -���������� x ������� ������ ���� ������
// width - ������ ���� � ��������
float EngineGL::getXnd(float Xw, int width, int X)
{
	

	return (((float)Xw / (((float)width - 1) / 2)) - 1);


}



// �������� ������� ���������� Ynd � �������� [-1 �� 1]
// Yw -������� ���������� � ��������
// Ynd - ������������� ���������� �� -1 �� 1
// Y -���������� Y ������� ������ ���� ������
// height - ������ ���� � ��������
float EngineGL::getYnd(float Yw, int height, int Y)
{
	
	return (((float)Yw - (((float)height - 1) / 2)) / (-1 * (((float)height - 1) / 2)));
}



// �������� ����
// width, height - ������ ������ 
// void EngineGL::ShowMenu(int width, int height)
void EngineGL::ShowMenu(float fElapsedTime)
{
	
	glPushMatrix();
		glLoadIdentity();
		glOrtho(0, width, height, 0, -1, 1);


	
		PROButton_Show(btn_exit);					// ������ ������ �����
		PROButton_Show(btn_ok);						// ������ ������ �����
		PROButton_Show(btn_setting);				// ������ ������ �����

	glPopMatrix();

	
}



// ������ ����
void EngineGL::drawWallpaper()
{
	glEnable(GL_TEXTURE_2D);				// ��������� ������������� ��������
	glBindTexture(GL_TEXTURE_2D, texture);	// ������ �������� ��������


	glColor3f(1, 1, 1);
	

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);


	glVertexPointer(3, GL_FLOAT, 0, vertex);
	glTexCoordPointer(2, GL_FLOAT, 0, texCoord);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);


	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	
	 glDisable(GL_TEXTURE_2D);				// ���������  ������������� ��������, ��� ���� ������� ������ ��������������� ���������� �� ���� �������� ����������
}



// ������ ������ 
void EngineGL::drawTextureButton(const float* pVertexButtonArr, const float* pTextureArr)
{
	glEnable(GL_TEXTURE_2D);				// ��������� ������������� ��������
	glBindTexture(GL_TEXTURE_2D, texture);	// ������ �������� ��������

	glColor3f(1, 1, 1);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	
			glVertexPointer(2, GL_FLOAT, 0, pVertexButtonArr);
			glTexCoordPointer(2, GL_FLOAT, 0, pTextureArr);
			glDrawArrays(GL_QUADS, 0, 4);
			
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);


	glDisable(GL_TEXTURE_2D);							// ���������  ������������� ��������, ��� ���� ������� ������ ��������������� ���������� �� ���� �������� ����������
}



// ��������� ��������
void EngineGL::DrawObjects(float fElapsedTime)
{
	
	drawWallpaper();				// ������ ����
	ShowMenu(fElapsedTime);
	
	if (bActive)
	{

		glPushMatrix();
		glLoadIdentity();
		glOrtho(0, width, height, 0, -1, 1);
		pFirework->OnUpdate(fElapsedTime);


		glPopMatrix();
	}
}



// ��������� ������ �����
void EngineGL::DrawFrame()
{
	
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);							// ������� ������ ������ ������ 
		glClear(GL_COLOR_BUFFER_BIT);
		m_tp2 = std::chrono::system_clock::now();						// �������� ������� ����� t2
		std::chrono::duration<float> elapsedTime = m_tp2 - m_tp1;		// �������� ����� ����������� �� ���������� �����
		m_tp1 = m_tp2;													// �������� �������� �������
		float fElapsedTime = elapsedTime.count();

		DrawObjects(fElapsedTime);										// ������ ������� � �����

		SwapBuffers(hdc);
		UpdateTitleBar(fElapsedTime);
	
}



// �������� ��������� ����� float
float EngineGL::RandomFloat(float fMax)
{
	return (((float)rand()) / ((float)RAND_MAX)) * fMax;
}



// ������������� OpenGL
void EngineGL::InitOpenGL(HWND& hWnd, HDC& hdc, HGLRC& hglrc)
{
	m_tp1 = std::chrono::system_clock::now();
	m_tp2 = std::chrono::system_clock::now();


	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;


	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	// �������� �������� ����������� hglrc 
	// �� ������� ���� hdc

	hdc = GetDC(hWnd);

	iPixelFormat = ChoosePixelFormat(hdc, &pfd);
	SetPixelFormat(hdc, iPixelFormat, &pfd);

	hglrc = wglCreateContext(hdc);

	if (hglrc)
		wglMakeCurrent(hdc, hglrc);


}





void pro::engine::EngineGL::InitOpenGL(const HWND& t_hwnd, const int t_width, const  int t_height)
{

	


	m_tp1 = std::chrono::system_clock::now();
	m_tp2 = std::chrono::system_clock::now();


	width = t_width;
	height = t_height;

	hwnd = t_hwnd;

	static PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;


	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	// �������� �������� ����������� hglrc 
	// �� ������� ���� hdc

	hdc = GetDC(hwnd);	
	iPixelFormat = ChoosePixelFormat(hdc, &pfd);
	SetPixelFormat(hdc, iPixelFormat, &pfd);
	hrc = wglCreateContext(hdc);
	if (hrc)
		wglMakeCurrent(hdc, hrc);


	set_vsync(bVSync);
}



// ��������� �������� ����
void EngineGL::ResizeOpenGL()
{

	RECT rc;
	GetClientRect(hwnd, &rc);
	glViewport(0, 0, rc.right, rc.bottom);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();


}



// ��������� �������� ���� ���������� ���������
void EngineGL::ResizeOpenGL2(LPARAM  lParam)
{


	width = LOWORD(lParam);
	height = HIWORD(lParam);
	glViewport(0, 0, width, height);
	glLoadIdentity();
	float k = width / (float)height;
	glOrtho(-k, k, -1, 1, -1, 1);


}



// �������� �������� ����������
void EngineGL::CloseOpenGL()
{

	if (hrc)
	{
		hdc = wglGetCurrentDC();
		wglMakeCurrent(NULL, NULL);
		ReleaseDC(hwnd, hdc);
		wglDeleteContext(hrc);
	}
}



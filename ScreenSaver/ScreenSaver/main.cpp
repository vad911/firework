//===========================================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			main.cpp 
// ����������:		���������. �������� � ����������� WinApi � OpenGL � Visual Studio 2019
// 					����������� ���������: � ������������ ������� �������� ����������:
//					OpenGL32.lib GLu32.lib 
// 					� Properties->Configuration Properties->Advanced->Charaster set
//					������ ����� : Use Unicode Charaster set
//					� Properties->Configuration Properties->Linker->System->SubSystem 
//					������ �����: Windows
//					�� �������������� ��������� ��� ������ ���� � ����� NVIDIA: 
//					wglext.h - ��� ������������� ������������� ������ bVSync
//
//===========================================================================================


// �������� ��������� ����� ����
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")



#include <windows.h>
#include <TCHAR.H>

#include "proFunctions.h"
#include "proGlobal.h"





// ���������� ����������
TCHAR  szClassName[] = _T("FireworkWinApp");			// ������������ ������
TCHAR  szClassName_Setting[] = _T("SettingWnd");		// ������������ ������





extern pro::engine::EngineGL engineGL;

// �������� ������� �������
int CALLBACK wWinMain
	(	
		_In_ HINSTANCE hInstance,				// handle ����������, ���������� ���������
		_In_opt_ HINSTANCE hPrevInstance,		// handle ����������� ���������� ����������, ��� �������������
		_In_ LPWSTR lpCmdLine,					// ��������� �� ��������� ������
		_In_ int nShowCmd						// ���������� � ����� ���� ����������� ���� ����� ���������� �� ������
	)
{

	
	MSG msg;			// ��������� 
	float theta = 0;
	
	int width	= 640;
	int height	= 640;
	bActive = true;

	
		// �������� ������� �� ���������������� �����
		// ������������ ����� ��� ������ ������� RegClass
		if (!RegClass(WndProc_Firework, szClassName, COLOR_WINDOW, WndProc_Setting, szClassName_Setting))
		{
			return EXIT_FAILURE;
		}
	
	// ������� ����
	HWND hwnd = CreateWindowEx(0, szClassName, _T("Firework"),
											
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW, 0, 0, width, height, nullptr, nullptr, hInstance, nullptr
						);

		// �������� ������� �� ������� ����
		if (!hwnd)
		{
			 return EXIT_FAILURE;
		}

	engineGL.InitOpenGL(hwnd, width, height);	// ������������� OpenGL
	engineGL.LoadTexture("city03.tga");

	ShowWindow(hwnd, nShowCmd);	// ������� �������� ���� �� ������
	UpdateWindow(hwnd);			// �������� ������� ���� ��������� WM_PAINT ��������������(�������� ����)
	
	hSettingWnd = CreateWindowEx(0, szClassName_Setting,	_T("���������"),
		WS_OVERLAPPED ,
		0,
		0,
		400,
		300,
		hwnd,
		NULL,
		hInstance,
		NULL);


	// �������� ������� �� ������� ���� ��������
	if (!hSettingWnd)
	{
		return EXIT_FAILURE;
	}
	
	

	// ������� ���� ��������� - ��������� ���������
	while (!bQuit)
	{
		// �������� ���� �� ��������� � �������
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// ��������� ��� �������� ��������� 
			if (msg.message == WM_QUIT)
			{
				bQuit = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			

		}
		
	}

	 engineGL.CloseOpenGL();	// ������������ �������� OpenGL
		
	 return (int)msg.wParam;
}

//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proGlobal.h 
// ����������:		���� ���������� ����������, ����������� ��������� ������,
//					��������� ������� ��� ��������� �� �������, 
//					����������� ����������� �������� - ������
//					
//=============================================================================

#ifndef GLOBAL_H
#define GLOBAL_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>


#include "pro4types.h"
#include "proEngineGL.h"
#include "proInterfaceUI.h"
#include "proScreenSaver.h"
#include "proImage.h"

extern pro::filesystem::IMAGE img;

extern pro::types::COLOR_RGBA colorRGBA_BLACK;
extern pro::types::COLOR_RGBA colorRGBA_WHITE;
extern pro::types::COLOR_RGBA colorRGBA_RED;
extern pro::types::COLOR_RGBA colorRGBA_GREEN;
extern pro::types::COLOR_RGBA colorRGBA_BLUE;
extern pro::types::COLOR_RGBA colorRGBA_CYAN;
extern pro::types::COLOR_RGBA colorRGBA_PURPLE;
extern pro::types::COLOR_RGBA colorRGBA_ORANGE;
extern pro::types::COLOR_RGBA colorRGBA_VIOLET;
extern pro::types::COLOR_RGBA colorRGBA_BLUEGREEN;
extern pro::types::COLOR_RGBA colorRGBA_BABYBLUE;
extern pro::types::COLOR_RGBA colorRGBA_LILAC;
extern pro::types::COLOR_RGBA colorRGBA_DARKGREY;
extern pro::types::COLOR_RGBA colorRGBA_DARKPURPLE;
extern pro::types::COLOR_RGBA colorRGBA_BRONZE;
extern pro::types::COLOR_RGBA colorRGBA_DARKBLUE;
extern pro::types::COLOR_RGBA colorRGBA_FORESTGREEN;
extern pro::types::COLOR_RGBA colorRGBA_BROWN;


extern pro::types::COLOR_RGB color_BLACK;
extern pro::types::COLOR_RGB color_WHITE;
extern pro::types::COLOR_RGB color_RED;
extern pro::types::COLOR_RGB color_GREEN;
extern pro::types::COLOR_RGB color_BLUE;
extern pro::types::COLOR_RGB color_CYAN;
extern pro::types::COLOR_RGB color_PURPLE;
extern pro::types::COLOR_RGB color_ORANGE;
extern pro::types::COLOR_RGB color_VIOLET;
extern pro::types::COLOR_RGB color_BLUEGREEN;
extern pro::types::COLOR_RGB color_BABYBLUE;
extern pro::types::COLOR_RGB color_LILAC;
extern pro::types::COLOR_RGB color_DARKGREY;
extern pro::types::COLOR_RGB color_DARKPURPLE;
extern pro::types::COLOR_RGB color_BRONZE;
extern pro::types::COLOR_RGB color_DARKBLUE;
extern pro::types::COLOR_RGB color_FORESTGREEN;
extern pro::types::COLOR_RGB color_BROWN;

extern pro::types::COLOR_RGB packColor[8];			// ����� ������


extern GLuint texture;								// ������ �������

// ���������� ��������
extern float vertex[];
extern float texCoord[];



// ������
extern pro::interfaceUI::PROButton btn_exit;		// ������ �����
extern pro::interfaceUI::PROButton btn_ok;			// ������ �����
extern pro::interfaceUI::PROButton btn_setting;		// ������ �����


extern HWND hSettingWnd;								// handle ���� �������

extern bool bQuit;										
extern bool bActive;									

// ���������� ������� ������
// ������ �����
extern float textBtn_exit1[];
extern float textBtn_exit2[];

// ������ ok
extern float textBtn_ok1[];
extern float textBtn_ok2[];

// ������ ���������
extern float textBtn_setting1[];
extern float textBtn_setting2[];


// ���������� ������ �� ������
extern float vertex_bt_exit[];
extern float vertex_bt_ok[];
extern float vertex_bt_setting[];


struct Boom
{
	bool bIneedABoom;								// �������, ��� ���������� ���������� �����, � ��� ����� ��� ����������  
	int mouse_X;									// ���������� ���� �� X
	int mouse_Y;									// ���������� ���� �� Y
};



extern Boom boom;

// =================================== ���� ������ � ������ ==============================
struct Salut_info
{
	int infoShots;						// ���������� ���������� �������
	double particles;					// ����� ���������� ������ ����������� � ��������
};

extern Salut_info info;			// ���������� � ������


#endif 
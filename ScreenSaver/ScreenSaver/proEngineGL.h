//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proEngineGL.h 
// ����������:		�������� ����������� ������� ��������� �����, ��������� �������,
//					��������� �������� � �.�. �������������, ������������ �������� OpenGL
//=============================================================================


#ifndef ENGINEGL_H
#define ENGINEGL_H


#include <iostream>

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "wglext.h"		// ������� ���� nvidia, ������ ���� � �������� �������, ��� ������������ ������������� ������ vSync -  wglSwapInterval
						// ������������ ������������� ��������, ��� ��������� ������ ����� ������ ��� ����� ��������� � ����������� ������

#include <chrono>


#include "proGlobal.h"
#include "pro4types.h"






extern float vertex[];
extern float texCoord[];



namespace pro
{

	namespace engine
	{
		
		struct EngineGL
		{
			

			HWND  hwnd;				// handle ����
			HDC   hdc;				// �������� ���������� 
			HGLRC hrc;				// handle GL ��������� ���������

			int width;				// ������ ����
			int height;				// ������ ����

			GLuint texture;			// ��������

			float theta;			// ���� ��������

			std::chrono::time_point<std::chrono::system_clock> m_tp1;		// ������ ���������� �������
			std::chrono::time_point<std::chrono::system_clock> m_tp2;		// ����� ���������� �������

			float		fFrameTimer;										// ������� ���������� ������� � ������ �������� ������						
		

			int			nFrameCount;				// ������� ������
			uint32_t	nLastFPS;					// ������� ���������� ���� ��������� ������ �� ������ ������
			float		fElapsedTime;				// ����� ��������� ����� ����� ��������� �������

			bool bVSync;							// ������������ �������������
			


			// �����������
			EngineGL();


			// �������� ������ FPS � �������� ����
			void UpdateTitleBar(float fElapsedTime);

		
			// �������� ������� ���������� Xw � ��������
			// Xw -������� ���������� � ��������
			// Xnd - ������������� ���������� �� -1 �� 1
			// X -���������� x ������� ������ ���� ������
			// width - ������ ���� � ��������
			int getXw(float Xnd, int width = 1920, int X = 0);


			// �������� ������� ���������� Yw � ��������
			// Yw -������� ���������� � ��������
			// Ynd - ������������� ���������� �� -1 �� 1
			// Y -���������� x ������� ������ ���� ������
			// height - ������ ���� � ��������
			int getYw(float Ynd, int height = 1024, int Y = 1024);


			// �������� ������� ���������� Xnd � �������� [-1 �� 1]
			// Xw -������� ���������� � ��������
			// Xnd - ������������� ���������� �� -1 �� 1
			// X -���������� x ������� ������ ���� ������
			// width - ������ ���� � ��������
			float getXnd(float Xw, int width = 1920, int X = 0);


			// �������� ������� ���������� Ynd � �������� [-1 �� 1]
			// Yw -������� ���������� � ��������
			// Ynd - ������������� ���������� �� -1 �� 1
			// Y -���������� x ������� ������ ���� ������
			// height - ������ ���� � ��������
			float getYnd(float Yw, int height = 1024, int Y = 1024);


			// ��������� / ���������� ������������ �������������
			// true -- VSync �������, false -- ��������
			void set_vsync(bool enabled); 


			// ������ ���������� �����
			// sizePixel - ������ ����� � ��������
			void DrawSquarePixel(float x, float y, pro::types::COLOR_RGB color, int sizePixel = 1);


			// ������ ������������ �����
			void DrawRoundPixel(float x, float y, pro::types::COLOR_RGB color, int sizePixel = 1);


			//�������� ��������
			void LoadTexture(const char* pFilename);


			// �������� ����
			// void ShowMenu(int width, int height);
			void ShowMenu(float fElapsedTime);


			// ��������� ��������
			void DrawObjects(float fElapsedTime);


			// ������ ����
			void drawWallpaper();

			
			// ������ ������ � ���������
			// pVertexButtonArr - ��������� �� ���������� ����� ���������� ������������� ������ (������ ������ �������� ������ ����� ����� � ������  ����� ������ ������� �������!)
			// pTextureArr - ��������� �� ���������� �������� (������ ����� ������� ����� � ��������������, ������ �� ������� �������!)
			void drawTextureButton(const float *pVertexButtonArr, const float *pTextureArr);


			// ��������� ������ �����
			void DrawFrame();


			// �������� ��������� ����� float
			float RandomFloat(float fMax);

			// ������������� OpenGL
			void InitOpenGL(HWND& hwnd, HDC& hdc, HGLRC& hrc);


			// ������������� OpenGL
			void InitOpenGL(const HWND& t_hwnd, const int t_width, const  int t_height);


			// ��������� �������� ����
			void ResizeOpenGL();

			// ��������� �������� ���� c ����������� ���������
			void ResizeOpenGL2(LPARAM  lParam);


			// �������� �������� ����������
			void CloseOpenGL();

			



		};
	}

}

#endif



//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proFunctions.h 
// ����������:		������� �������������� ������� ����, �������� � ��������
//					
//=============================================================================

#ifndef FUNCTIONS_H
#define FUNCTIONS_H


#include <windows.h>


#include <TCHAR.H>
#include <ctime>
#include <cstdlib>	// ��� rand	
#include <iostream>

#include "proScreenSaver.h"


// ����������� ������ ����
ATOM CALLBACK RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground, WNDPROC Proc_child, LPCTSTR szName_child);


// ������� ��������� ������� ���� �� ����������� ���������
// hWnd - ���������� ���� ��������
// uMsg - ���������
// wParam, lParam - ��������� ���������� ��������� 
LRESULT CALLBACK WndProc_Firework(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM  lParam);


// ������� ���������� ��� ��������� ���� Setting ��������
LRESULT CALLBACK WndProc_Setting(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


// �������� ������� ����, ���������� X Y
// mouse_posX - ���������� X
// mouse_posY - ���������� Y
void getMousePos(LPARAM lParam, int& mouse_posX, int& mouse_posY );



#endif
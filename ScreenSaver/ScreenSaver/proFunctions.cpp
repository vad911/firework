#include "proFunctions.h"
#include "proEngineGL.h"
#include "proGlobal.h"

#include <sstream>



extern pro::engine::EngineGL engineGL;
extern pro::screensaver::Firework* pFirework;


extern TCHAR  szClassName_Setting[];		// ������������ ������


// ������������ �����
ATOM CALLBACK RegClass(WNDPROC Proc, LPCTSTR szName, UINT brBackground, WNDPROC Proc_child, LPCTSTR szName_child)
{
	bool result1 = false;
	bool result2 = false;

	WNDCLASS wc;
	//wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// ����� ���� ����� ������
	wc.style = CS_OWNDC;	// ����� ���� ����� ������
	wc.cbClsExtra = 0;									// �������������� ������ ��� ������	
	wc.cbWndExtra = 0;									// �������������� ������ ��� ������� ����
	wc.lpfnWndProc = Proc;								// ��� ������� ����
	wc.hInstance = GetModuleHandle(NULL);				// ���������� ���������� ���������� exe ����
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);			// ���������� ������ ��� ������� ����
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			// ���������� ������� ��� ������� ����
	//wc.hbrBackground = (HBRUSH)(brBackground + 1);	// ���������� ����� ��� ���� ����
	wc.hbrBackground = NULL;							// ��� ��������� ������� ����� ������ ������������
	wc.lpszMenuName = (LPCTSTR)NULL;					// ��� ������� ����
	wc.lpszClassName = szName;							// ��� ������������ ������

	result1 = (RegisterClass(&wc));						// ������������ ����� � ������������ �������


	// ����������� ��������� ����
	WNDCLASS wcChild1 = wc;

	wcChild1.lpfnWndProc = (WNDPROC)Proc_child;
	wcChild1.lpszClassName = szName_child;
	wcChild1.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcChild1.hIcon = LoadIcon(NULL, IDI_ASTERISK);
	wcChild1.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcChild1.lpszMenuName = NULL;
	wcChild1.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wc.cbClsExtra = 0;// ��� �������������� ������ ������
	wc.cbWndExtra = 0;


	
	result2 = RegisterClass(&wcChild1);

	return (result1 && result2) ;
}

//������� ��������� ������� ���� �� ��� ����������� ���������
LRESULT CALLBACK WndProc_Firework(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM  lParam)
{
	
	
	switch (uMsg)
	{
	// ��� �������� ����
	case WM_CREATE:		{
							srand( static_cast<unsigned int>(time(0)));
							pFirework = new pro::screensaver::Firework;
						
							break;
						
						}
							
				 
	
	// ��������� �������� ���� 
	case WM_SIZE:		{
							//engineGL.ResizeOpenGL2(lParam);	// ������ � ����������� ���������
							//engineGL.ResizeOpenGL();
						
							break;
						}



	// ����������� ����
	case WM_PAINT:		{
													
							engineGL.DrawFrame();
							break;
						}
					


				
	// ������ ����� ������ ����
	case WM_LBUTTONDOWN:{
		
														
							 if (ClickOnButtonRect(LOWORD(lParam), HIWORD(lParam), btn_exit) == true)
							{
									 btn_exit.key_state = pro::interfaceUI::KEYMODE::PRESSED;
									 break;
							}

							 // �������� ���� Setting
							if ((ClickOnButtonRect(LOWORD(lParam), HIWORD(lParam), btn_setting) == true) )
							 {

								
										 btn_setting.key_state = pro::interfaceUI::KEYMODE::PRESSED;

										 ShowWindow(hSettingWnd, SW_NORMAL);
										 UpdateWindow(hSettingWnd);
										 break;
									
							 }

							 // ������ �����
							if (ClickOnButtonRect(LOWORD(lParam), HIWORD(lParam), btn_ok) == true)
							 {
								 btn_ok.key_state = pro::interfaceUI::KEYMODE::PRESSED;
								 if (bActive) { bActive = false; }
								 else { bActive = true; }
								
								 break;

							 }
							 // ��� ������ ����� ������ ���� � ��� ������ �� �� ������ ��, ���������� � �����, ������ ���������� ���������� ����� ������ � ���� �����
							{

								 boom.bIneedABoom = true;							// ��������, ��� ����� ����� � ���� �����
								 getMousePos(lParam, boom.mouse_X, boom.mouse_Y);	// �������� ������� ���� � ���������� boom.mouse_X, boom.mouse_Y
								 break;
							 }
							 
							break;
						}

		 // �������� �� ������� ���������� ����� ������ ����
	case WM_LBUTTONUP:
						{

							
							if (btn_ok.key_state == pro::interfaceUI::KEYMODE::PRESSED)
								{
									btn_ok.key_state = pro::interfaceUI::KEYMODE::RELEASED;
									break;
								}

							

							else if (btn_setting.key_state == pro::interfaceUI::KEYMODE::PRESSED)
							{
								btn_setting.key_state = pro::interfaceUI::KEYMODE::RELEASED;
								break;
							}

							else if (btn_exit.key_state == pro::interfaceUI::KEYMODE::PRESSED)
							{
								btn_exit.key_state = pro::interfaceUI::KEYMODE::RELEASED;

								delete pFirework;
								PostQuitMessage(EXIT_SUCCESS);
								break;
							}

							

							break;
						}
	

	// �� ����������� ����
	case WM_DESTROY:	{
													
							delete pFirework;
							PostQuitMessage(EXIT_SUCCESS);
							break;
							
						}


						
	default: {return DefWindowProc(hWnd, uMsg, wParam, lParam); }

	}


	return 0;
}

	
LRESULT CALLBACK WndProc_Setting(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	PAINTSTRUCT ps;
	HDC hDC;
	HINSTANCE hInst;

	static HWND hBtn1, hBtn2; // ���������� ������
	static HWND hEdt1, hEdt2, hEdt3; // ����������� ����� ��������������
	


	std::wstringstream ss;
	// ������� ������ ������������� � ������ �����
	std::wstring str_val1;		// min
	std::wstring str_val2;		// max
	std::wstring str_val3;		// T delay
	ss << pFirework->get_Min();
	ss >> str_val1;
	ss.clear();

	ss << pFirework->get_Max();
	ss >> str_val2;
	ss.clear();

	ss << pFirework->get_fDelay();
	ss >> str_val3;
	ss.clear();



	// ���������� ���������
	switch (message)
	{
	case	 WM_DESTROY: {
							PostQuitMessage(0);
							break;
						}
	case WM_CREATE:		{
								hInst = ((LPCREATESTRUCT)lParam)->hInstance; // ���������� ����������


								// ������� � ���������� ������ ���� �������������� "min"
								hEdt1 = CreateWindowEx(0, _T("edit"), str_val1.c_str(),
									WS_CHILD | WS_VISIBLE | WS_BORDER | ES_RIGHT, 90, 50, 120, 20, 
									hWnd, 0, hInst, NULL);
								ShowWindow(hEdt1, SW_SHOWNORMAL);
								// ������� � ���������� ������ ���� �������������� "max"
								hEdt2 = CreateWindowEx(0, _T("edit"), str_val2.c_str(),
									WS_CHILD | WS_VISIBLE | WS_BORDER | ES_RIGHT, 90, 80, 120, 20,
									hWnd, 0, hInst, NULL);
								ShowWindow(hEdt2, SW_SHOWNORMAL);
								// ������� � ���������� ������ ���� �������������� "T delay"
								hEdt3 = CreateWindowEx(0, _T("edit"), str_val3.c_str(),
									WS_CHILD | WS_VISIBLE | WS_BORDER | ES_RIGHT, 140, 110, 70,	20,
									hWnd, 0, hInst, NULL);
								ShowWindow(hEdt3, SW_SHOWNORMAL);
						
								// ������� � ���������� ������ "���������"
								hBtn1 = CreateWindowEx(0, _T("button"), _T("���������"),
										WS_CHILD | WS_VISIBLE | WS_BORDER, 	50, 200, 120, 30,
										hWnd, 0, hInst, NULL);
								ShowWindow(hBtn1, SW_SHOWNORMAL);
								// ������� � ���������� ������ "Ok"
								hBtn2 = CreateWindowEx(0, _T("button"), _T("OK"),
										WS_CHILD | WS_VISIBLE | WS_BORDER, 200, 200, 120, 30,
										hWnd, 0, hInst, NULL);
								ShowWindow(hBtn2, SW_SHOWNORMAL);

								break;

						}

	case WM_COMMAND:		{

								if (lParam == (LPARAM)hBtn2)    // ���� ������ �� ������ "OK"
								{
									ShowWindow(hWnd, SW_HIDE);	// ������ ����
									break;
								}

								if (lParam == (LPARAM)hBtn1)    // ���� ������ �� ������ "���������"
								{
									int tMin;
									int tMax;
									float tDelay;
									int len;
									// min
									len = GetWindowTextLength(hEdt1) + 1;
									TCHAR* buff1 = new TCHAR[len];
									len = GetWindowText(hEdt1, buff1, len);
									str_val1 = buff1;

									ss << str_val1;
									ss >> tMin;
									pFirework->set_Min(tMin);
									ss.clear();
									delete[] buff1;


									// max
									len = GetWindowTextLength(hEdt2) + 1;
									TCHAR* buff2 = new TCHAR[len];
									len = GetWindowText(hEdt2, buff2, len);
									str_val2 = buff2;

									ss << str_val2;
									ss >> tMax;
									pFirework->set_Max(tMax);
									ss.clear();
									delete[] buff2;

									// tDelay
									len = GetWindowTextLength(hEdt3) + 1;
									TCHAR* buff3 = new TCHAR[len];
									len = GetWindowText(hEdt3, buff3, len);
									str_val3 = buff3;

									ss << str_val3;
									ss >> tDelay;
									if (tDelay <= 0.0f) { tDelay = 1.0f; }

									pFirework->set_fDelay(tDelay);
									ss.clear();
									delete[] buff3;

									delete  pFirework;

									pFirework = new pro::screensaver::Firework(tMin, tMax, tDelay);
									break;
								}


							}



	case WM_PAINT:		{
							TCHAR str01[] = _T("���������� ��������� � ������.");
							TCHAR str02[] = _T("Min:");
							TCHAR str03[] = _T("Max:");
							TCHAR str04[] = _T("T ��������:");

							hDC = BeginPaint(hWnd, &ps); // ������ �����������
							TextOut(hDC, 50, 20, str01, ARRAYSIZE(str01)); // ����� ��������� ���������
							TextOut(hDC, 50, 50, str02, ARRAYSIZE(str02)); // ����� ��������� ���������
							TextOut(hDC, 50, 80, str03, ARRAYSIZE(str03)); // ����� ��������� ���������
							TextOut(hDC, 50, 110, str04, ARRAYSIZE(str04)); // ����� ��������� ���������
						
							EndPaint(hWnd, &ps); // ����� �����������

							break;
						}

		 // ���������� ���������
		// ��������� ��������� �� ���������
	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


void getMousePos(LPARAM lParam, int& mouse_posX, int& mouse_posY)
{
	mouse_posX = LOWORD(lParam);
	mouse_posY = HIWORD(lParam);
}

#include "pro4types.h"

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

using namespace pro::types;


//===============================  COLOR_RGBA  ===============================

pro::types::COLOR_RGBA::COLOR_RGBA()
{
	m_r = 0;
	m_g = 0;
	m_b = 0;
	m_a = 0;
}

COLOR_RGBA::COLOR_RGBA(float t_r, float t_g, float t_b, float t_a) : m_r(t_r), m_g(t_g), m_b(t_b), m_a(t_a) {}


COLOR_RGBA::COLOR_RGBA(const COLOR_RGBA& other_color)
{
	m_r = other_color.m_r;
	m_g = other_color.m_g;
	m_b = other_color.m_b;
	m_a = other_color.m_a;

}

COLOR_RGBA& COLOR_RGBA::operator=(const COLOR_RGBA& other_color)
{
	if (this != &other_color)
	{
		m_r = other_color.m_r;
		m_g = other_color.m_g;
		m_b = other_color.m_b;
		m_a = other_color.m_a;
	}
	return *this;
}

bool COLOR_RGBA::operator==(const COLOR_RGBA& other_color)
{
	if ((m_r == other_color.m_r) && (m_g == other_color.m_g) && (m_b == other_color.m_b) && (m_a == other_color.m_a))
	{
		return true;
	}
	return false;

}


void COLOR_RGBA::setColor() { glColor4f(m_r, m_g, m_b, m_a); }

void COLOR_RGBA::clear() { m_r = m_g = m_b = m_a = 0; }


//===============================  COLOR_RGB  ===============================

pro::types::COLOR_RGB::COLOR_RGB()
{
	m_r = 0;
	m_g = 0;
	m_b = 0;
}

COLOR_RGB::COLOR_RGB(float t_r, float t_g, float t_b) : m_r(t_r), m_g(t_g), m_b(t_b) {}

COLOR_RGB::COLOR_RGB(const COLOR_RGB& other_color)
{
	m_r = other_color.m_r;
	m_g = other_color.m_g;
	m_b = other_color.m_b;
}

COLOR_RGB& COLOR_RGB::operator=(const COLOR_RGB& other_color)
{
	if (this != &other_color)
	{
		m_r = other_color.m_r;
		m_g = other_color.m_g;
		m_b = other_color.m_b;

	}
	return *this;
}


bool COLOR_RGB::operator==(const COLOR_RGB& other_color)
{
	if ((m_r == other_color.m_r) && (m_g == other_color.m_g) && (m_b == other_color.m_b))
	{
		return true;
	}
	return false;

}


void COLOR_RGB::setColor() 
{
		
		glColor3f
		(
			m_r,
			m_g,
			m_b
		); 
};

void COLOR_RGB::clear() { m_r = m_g = m_b = 0; };


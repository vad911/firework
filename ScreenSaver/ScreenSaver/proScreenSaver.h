//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proScreenSaver.h 
// ����������:		�������� �� �������� ������� ���������, ��������� ��� ������
//					
//					
//					
//=============================================================================

#ifndef PARTICLE_H
#define PARTICLE_H

#include <ctime>
#include <cstdlib>	// ��� rand

#include <list>
#include <vector>

#include "pro4types.h"







namespace pro
{

	namespace screensaver {


		class Firework
		{

		public:

			float posOrigin_x;	// ���������� x �������  ����������
			float posOrigin_y;	// ���������� y �������  ����������


			int m_Min;			// min ���������� ��������� � ������ ������
			int m_Max;			// max ���������� ��������� � ������ ������
			float m_MaxTimeDelay;	// ������������ ����� ��������


			enum ecolor { cBLACK, cWHITE, cRED, cGREEN, cBLUE, cCYAN, cPURPLE, cORANGE, cVIOLET, cBLUEGREEN, cBABYBLUE, cLILAC, cDARKGREY, cDARKPURPLE, cBRONZE, cDARKBLUE, cFORESTGREEN };

			Firework(int tm_Min = 100, int tm_Max = 800, float t_MaxTimeDelay = 2,  float position = 300.0f, float ground = 450.0f);

			void set_Min(const int t_Min) { m_Min = t_Min; }
			int get_Min() const { return m_Min; }

			void set_Max(const int t_Max) { m_Max = t_Max; }
			int get_Max() const { return m_Max; }

			void set_fDelay(const float t_fDelay) { fDelay = t_fDelay; }
			float get_fDelay() const { return fDelay; }


			// ��������� �������
			struct Particle
			{
				// ��������� ����������
				float x = 0;			// ���������� �� ��� x
				float y = 0;			// ���������� �� ��� y

				float vx = 0;			// �������� �� ��� x
				float vy = 0;			// �������� �� ��� y
				float fuse = 0;			// ���������� ������������ ����� ����� ������, ����� ����� ��������� ��������������

				float lifetime = 0;		// ����� ����� �������
				float fDrag = 0;		// ������������� �� ��� �

				int size_shot;			// ������� �����
				pro::types::COLOR_RGB color_pack[17];	// ����� ������ ��� �����������
				int color_num;							// ������ �����
				pro::types::COLOR_RGB color;			// ����
				float fGravity = 10.0f;	// ��������� ����������

				



			};



			class Shot : public Particle
			{

			public:

				::std::vector<Particle> vec_Stars;	// ������ ������ ���������� ����� ������ ������
				int starcount = 100;				// ���-�� ������ �� ������� ����� ����������� �����
				bool bhasExploded = false;			// �������, ��� ����� ���������
				bool bExpired = false;				// �������, ��� �������  �������


				Shot();

				// �����������
				Shot(const float position = 100.0f, const float ground = 100.0f, const int size = 600, const float  t_fuse = 2, const bool t_bhasExploded = false);

				// �������� ��������� ���������� ����������
				void Update(float fElapsedTime);

				// ���������
				void DrawSelf(float fElapsedTime);


			};


			::std::list<Shot> listShots;				// ������ ���������
			float fDelay; 								// �������� ����� �������� ���������� ��������


			bool OnUpdate(float fElapsedTime);

		
			

		};
	}
}

#endif
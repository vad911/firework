#include "proScreenSaver.h"
#include "proEngineGL.h"

#include "proGlobal.h"

extern pro::engine::EngineGL engineGL;



extern int elem_Min;
extern int elem_Max;
extern float fTdelay;




pro::screensaver::Firework::Shot::Shot()
{
	color = color_GREEN;
	color_num = (rand() % 8);

	color_pack[0] = color_BLACK;
	color_pack[1] = color_WHITE;
	color_pack[2] = color_RED;
	color_pack[3] = color_GREEN;
	color_pack[4] = color_BLUE;
	color_pack[5] = color_CYAN;
	color_pack[6] = color_PURPLE;
	color_pack[7] = color_ORANGE;
	color_pack[8] = color_VIOLET;
	color_pack[9] = color_BLUEGREEN;
	color_pack[10] = color_BABYBLUE;
	color_pack[11] = color_LILAC;
	color_pack[12] = color_DARKGREY;
	color_pack[13] = color_DARKPURPLE;
	color_pack[14] = color_BRONZE;
	color_pack[15] = color_DARKBLUE;
	color_pack[16] = color_FORESTGREEN;

}

pro::screensaver::Firework::Shot::Shot(const float position, const float ground, const int size, const float  t_fuse, const bool t_bhasExploded)
{
	
	info.infoShots = info.infoShots + 1;		// ������� ���������� ��������

	x = position;
	y = ground;
	vy = -100.0f;
	vx = engineGL.RandomFloat(50.0f) - 30.0f;		// ������ �������� �� x
	fuse = engineGL.RandomFloat(6.0f);				// ���������� ������������ ����� ����� ������, ����� ����� ��������� ��������������
	color_num = cWHITE;
	starcount = size;
	size_shot = 4;
	color_num = (rand() % 8);
	bhasExploded = t_bhasExploded;
	fuse = t_fuse;

	
	color_pack[0] = color_BLACK;
	color_pack[1] = color_WHITE;
	color_pack[2] = color_RED;
	color_pack[3] = color_GREEN;
	color_pack[4] = color_BLUE;
	color_pack[5] = color_CYAN; 
	color_pack[6] = color_PURPLE;
	color_pack[7] = color_ORANGE;
	color_pack[8] = color_VIOLET;
	color_pack[9] = color_BLUEGREEN;
	color_pack[10] = color_BABYBLUE;
	color_pack[11] = color_LILAC;
	color_pack[12] = color_DARKGREY;
	color_pack[13] = color_DARKPURPLE;
	color_pack[14] = color_BRONZE;
	color_pack[15] = color_DARKBLUE;
	color_pack[16] = color_FORESTGREEN;
	


}

void pro::screensaver::Firework::Shot::Update(float fElapsedTime)
{
	
	float fGravity = 25.0f;	// ��������� ����������
	fDrag = 0.999f;			// ������������� �� ��� �
	// ������� ����� ����� �������
	lifetime = lifetime + fElapsedTime;

	if (lifetime <= fuse)
	{
		// ���� ��� ��������� � �����	� ���� �������� shot, �� ������		
	}
	else
	{ 
		if (!bhasExploded)
		{
			// ������ ��� ����������
			bhasExploded = true;
			color_num = rand() % 7 + 8;
			color = color_pack[color_num];
			info.particles = info.particles + starcount;	// ���������� ���������� ������ � ������
			// ����� ����� ������ ����������� - !!! ����� !!!
			for (int i = 0; i < starcount; ++i)
			{
				Particle s;
				s.fuse = 1.0f + engineGL.RandomFloat(5.0f);		// ������� ����� ����� �������� ���������� ����� ������
				s.x = x;
				s.y = y;
				float angle = engineGL.RandomFloat(2.0f * 3.14159f); // ���� �� �������� ����� �������� ������� 360 ����
				float power = engineGL.RandomFloat(50.0f);			// ���� � ������� ����� ���������� �������

				s.vx = cosf(angle) * power;
				s.vy = sinf(angle) * power;

				 color_num = (rand() % 7) + 1;
				 color = color_pack[color_num];
			

				vec_Stars.push_back(s);

			}
		}

		else
		{
			for (auto& s : vec_Stars)
			{
				s.lifetime = s.lifetime + fElapsedTime;
				s.vx = s.vx * fDrag;		// ������������� �������� �������� vx c ������ ����������, ���� �������������
				s.x = s.x + (s.vx * fElapsedTime);
				s.y = s.y + ((s.vy + fGravity) * fElapsedTime);
			}

		}
	}

	// ��������� ������� ������� �� x
	x = x + (vx * fElapsedTime);
	y = y + ((vy + fGravity) * fElapsedTime);


}

void pro::screensaver::Firework::Shot::DrawSelf(float fElapsedTime)
{

	
	if (lifetime < fuse)
	{
		

		// �������� ���� ������� �������� shot �� ��� ������
		 engineGL.DrawRoundPixel(x, y, color_ORANGE, size_shot);
		
	}
	else
	{
		
		bExpired = true;
		for (auto& s : vec_Stars)
		{
			

			if (s.lifetime <= s.fuse)
			{
				bExpired = false;
				
				// �������� ���� �������
				color_num = color_num - (s.lifetime >= s.fuse * 0.75);				// ��������� ���� �����
						
				engineGL.DrawRoundPixel(s.x, s.y, color, size_shot);				// �������� ���� �������
			}


		}
	}

	
	
	Update(fElapsedTime);

	
}

pro::screensaver::Firework::Firework(int tm_Min, int tm_Max, float tm_fTDelay, float t_position , float t_ground ) : m_Min(tm_Min), m_Max(tm_Max), m_MaxTimeDelay(tm_fTDelay)
{
	posOrigin_x = t_position;
	posOrigin_y = t_ground;

	
						
}

bool pro::screensaver::Firework::OnUpdate(float fElapsedTime)
{
	
	
		// �������� ����� �� ����������� ����� � ��������� �����
		if (boom.bIneedABoom)
		{

			int numStars = rand() % m_Max + m_Min;
			listShots.push_back({ (float)boom.mouse_X, (float)boom.mouse_Y,  numStars, 0.01f, false });

			boom.bIneedABoom = false;	// ������� ������� ������� �����
		}
		


		// ������������� �������� ������� ���������
		fDelay = fDelay - fElapsedTime;			// ����������� �������� ����� ���������� ���������
	
		if (fDelay <= 0.0f)
		{
			// fDelay = ((float)rand()) / ((float)RAND_MAX) * 4.0f + 0.100f;
			fDelay = ((float)rand()) / ((float)RAND_MAX) * m_MaxTimeDelay + 0.100f;
			listShots.push_back({ posOrigin_x, posOrigin_y, rand() % m_Max + m_Min });
		}
		
		//������� ������ ���������
		for (auto& f : listShots)
		{
			f.Update(fElapsedTime);			// ��������� ���������� , �������� ���������� �����
			f.DrawSelf(fElapsedTime);		// ������ �� ������
		}

		listShots.remove_if([](const Shot& s) {return s.bExpired; });


		return true;
}

//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proImage.h 
// ����������:		���������� ���������� � �������, �������� ����������� ��������
//					
//					
//					
//=============================================================================

#ifndef IMAGE_H
#define IMAGE_H


#include <stdio.h>


#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>


#include <algorithm>


namespace pro
{
	namespace filesystem
	{


		typedef unsigned long  ulong;
		typedef unsigned short ushort;
		typedef unsigned char  uchar;
		typedef unsigned int   uint;

		// ������ 54 ����� ��� ������������
#pragma pack(push,1)
		struct header_bmp
		{
			short bmpid;		// 'BM'
			ulong filesize;		// ������ ����� � ������
			ulong tmp1;			// �� ������������
			ulong img_data;		// �������� �� ������ ����� �� ����� ��� ���������� ����� �����������
			ulong header_size;	// ������ ��������� � ������ (50 ����)
			long width;			// ������ ����������� � ��������
			long height;		// ������ ����������� � ��������
			ushort planes;		// ���������� ����������
			ushort bpp;			// ����������� ��� �� �������
			ulong compression;	// ��� ����������
			ulong img_dataSize;	// ������ ������ ����������� � ������, ����������� �� �������� ����� ��� 0
			ulong HR;			// ���������� � �������� ��������������
			ulong VR;			// ���������� � �������� ������������
			ulong color;		// ���������� ������ �������
			ulong imcolor;		// ���������� ����� ������ (���� � ������ ������

			void init()		//�������������
			{
				bmpid = 'BM';
				header_size = 40;
				tmp1 = 0;
				compression = 0;
				imcolor = 0;
				HR = 0;
				VR = 0;
				planes = 1;
			}

		};


		struct header_tga
		{
			uchar idlen;
			uchar typepal;	// ��� �������
			uchar typeimg;	// ��� �����������
			ushort colorstart;	// ����� ������� �������� � �������
			ushort colornum;	// ���������� ������������ ��������� � �������
			uchar colorbpp;		// ���������� ��� �� ������� �������� �������
			ushort x;			// ��������� ����������� �� ������ �� ��� x
			ushort y;			// ��������� ����������� �� ������ �� ��� y
			ushort width;		// ������ �����������
			ushort height;		// ������ �����������
			uchar bpp;			// ���������� ��� �� �������
			uchar ImageDecriptor;	


			void init()
			{
				idlen = 0;
				typepal = 0;
				typeimg = 0;
				colorstart = 0;
				colornum = 0;
				colorbpp = 0;
				x = 0;
				y = 0;
				width = 0;
				height = 0;
				bpp = 0;
				ImageDecriptor = 0;

			}


		};
		#pragma pack(pop)	// ��������� �������� �������� ��� ������������

		struct IMAGE 
		{
			int width;		// ������ �����������
			int height;		// ������ �����������
			int bpp;		// ���������� ��� �� �������
			int sizepal;	// ������ �������
			int bpppal;		// ���������� ��� �� �������
			uchar* inddata;	// ������ �����������
			uchar* pal;		// ������ �������

			void init(int t_width = 320, int t_height = 200, int t_bpp = 24, int t_sizepal = 0, int t_bpal = 32);
			
			IMAGE() 
			{
				width = 0;
				height = 0;
				bpp = 0;
				sizepal = 0;
				bpppal = 0;
				inddata = 0;
				pal = 0;
			}

			~IMAGE()
			{
				clear();
			}


			//
			// ��������� ����������� �������� ����������� ������
			// dst - ���� ����������� ��������� �� ����� ������
			// ic - ������� ���� ����������� �� ���� ���
			// ia - �� ������� ���� �������� dst
			// max - ������� ��� ���������
			// order - ������ ������
			// ��������� �� �������� ����
			void fload_data(uchar* dst, int ic, int ia, int max, int order, FILE* f);
			
			// src - ������ ����������� ������  ��� ������ ����� 
			// ic - ������� ���� ����������� �� ���� ���
			// ia - �� ������� ���� �������� dst
			// max - ������� ��� ���������
			// order - ������ ������
			// ��������� �� �������� ����
			void fsave_data(uchar* src, int ic, int ia, int max, int order, FILE* );

			// �������� bmp �����
			int loadbmp(const char* filename);

			// �������� tga �����
			int loadtga(const char* filename);

			// ��������� ������ �����������
			int imageSize();

			//������������ ������
			void clear();

		};


		
	}
}
#endif

#include "proGlobal.h"




 // ��������� ��������� �����
 using namespace pro::types;
 using namespace pro::interfaceUI;
 using namespace pro::engine;
 using namespace pro::screensaver;
 using namespace pro::filesystem;

 // �������� ���������� ����������
Firework *pFirework;

GLuint texture;
IMAGE img;							//	 ����������� �������� �������� � img, ����� ��������� ������ � ��� ���������� ��� ��������



COLOR_RGBA colorRGBA_BLACK(0.0f, 0.0f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_WHITE(1.0f, 1.0f, 1.0f, 1.0f);
COLOR_RGBA colorRGBA_RED(1.0f, 0.0f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_GREEN(0.0f, 1.0f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_BLUE(0.0f, 0.0f, 1.0f, 1.0f);
COLOR_RGBA colorRGBA_CYAN(0.5f, 1.0f, 1.0f, 1.0f);			
COLOR_RGBA colorRGBA_PURPLE(1.0f, 0.0f, 1.0f, 1.0f);
COLOR_RGBA colorRGBA_ORANGE(1.0f, 0.5f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_VIOLET(0.5f, 0.5f, 0.5f, 1.0f);
COLOR_RGBA colorRGBA_BLUEGREEN(0.0f, 0.5f, 0.5f, 1.0f);
COLOR_RGBA colorRGBA_BABYBLUE(0.0f, 0.5f, 1.0f, 1.0f);
COLOR_RGBA colorRGBA_LILAC(1.0f, 0.5f, 1.0f, 1.0f);
COLOR_RGBA colorRGBA_DARKGREY(0.1f, 0.1f, 0.1f, 1.0f);
COLOR_RGBA colorRGBA_DARKPURPLE(0.1f, 0.0f, 0.1f, 1.0f);
COLOR_RGBA colorRGBA_BRONZE(0.1f, 0.1f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_DARKBLUE(0.0f, 0.1f, 0.1f, 1.0f);
COLOR_RGBA colorRGBA_FORESTGREEN(0.0f, 0.1f, 0.0f, 1.0f);
COLOR_RGBA colorRGBA_BROWN(0.1f, 0.0f, 0.0f, 1.0f);


COLOR_RGB color_BLACK(0.0f, 0.0f, 0.0f);
COLOR_RGB color_WHITE(1.0f, 1.0f, 1.0f);
COLOR_RGB color_RED(1.0f, 0.0f, 0.0f);
COLOR_RGB color_GREEN(0.0f, 1.0f, 0.0f);
COLOR_RGB color_BLUE(0.0f, 0.0f, 1.0f);
COLOR_RGB color_CYAN(0.5f, 1.0f, 1.0f);
COLOR_RGB color_PURPLE(1.0f, 0.0f, 1.0f);
COLOR_RGB color_ORANGE(1.0f, 0.5f, 0.0f);
COLOR_RGB color_VIOLET(0.5f, 0.5f, 0.5f);
COLOR_RGB color_BLUEGREEN(0.0f, 0.5f, 0.5f);
COLOR_RGB color_BABYBLUE(0.0f, 0.5f, 1.0f);
COLOR_RGB color_LILAC(1.0f, 0.5f, 1.0f);
COLOR_RGB color_DARKGREY(0.1f, 0.1f, 0.1f);
COLOR_RGB color_DARKPURPLE(0.1f, 0.0f, 0.1f);
COLOR_RGB color_BRONZE(0.1f, 0.1f, 0.0f);
COLOR_RGB color_DARKBLUE(0.0f, 0.1f, 0.1f);
COLOR_RGB color_FORESTGREEN(0.0f, 0.1f, 0.0f);




EngineGL engineGL;			// ������� openGL


float MaxTimeDelay;			// ������������ ����� �������� ���� ��������


float vertex[] =	{ -1, -1, 0,   1, -1, 0,   1, 1, 0,   -1, 1, 0 };
float texCoord[] = { 0,0.217, 1,0.217, 1,1,  0,1 };


// ==================================== ���������� ������ ������� � ������ [begin] ==========================================================
// ���������� ������� ������
// ������ �����

 float textBtn_exit1[] = { 0.0, 0.0458, 0.0937,0.0458,  0.0937,0.0,  0.0,0.0,      };		// !!!������� ����������
 float textBtn_exit2[] = { 0, 0.156, 0.0937, 0.156, 0.0937,0.1093, 0, 0.1093      };


// ������ ok
float textBtn_ok1[]		= { 0.107,0.0458,    0.201,0.0458,    0.201,0.0,      0.107,0.0   };
float textBtn_ok2[]		= { 0.107,0.156,     0.201,0.156,     0.201,0.109,    0.107,0.109  };

// ������ ���������
float textBtn_setting1[] = { 0.215,0.0458,   0.309,0.0458,    0.309,0.0,       0.215,0.0   };
float textBtn_setting2[] = { 0.215,0.156,    0.309,0.156,     0.309,0.109,     0.215,0.109 };


// ���������� ������ �� ������
 float vertex_bt_exit[]	= { 10,470, 100,470,  100, 500, 10, 500 };			// { -0.968701, -0.471049, -0.687011, -0.471049, -0.687011, -0.564945, -0.968701, -0.564945 }
 float vertex_bt_ok[]		= { 110,470, 200,470 , 200, 500, 110, 500 };	// {-0.655712, -0.471049, -0.374022, -0.471049, -0.374022, -0.564945, -0.655712, -0.564945, }
 float vertex_bt_setting[]= { 210,470, 300,470 , 300, 500, 210, 500 };		//  {-0.342723, -0.471049, -0.0610329, -0.471049, -0.0610329, -0.564945, -0.342723, -0.564945}




 // ==================================== ���������� ������ ������� � ������ [end] ==========================================================



Boom boom{ false, 0, 0 };


HWND hSettingWnd;										// ���� Setting ��������

bool bQuit;			// �������,  ��� ��������� ���������
bool bActive;		// ������� ������������� ��������� �����

PROButton btn_exit			= { "exit",		{10,470, 100,470,  100, 500, 10, 500},   textBtn_exit1, textBtn_exit2, pro::interfaceUI::KEYMODE::RELEASED };		// ������ �����
PROButton btn_ok			= { "ok",		{110,470, 200,470 , 200, 500, 110, 500}, textBtn_ok1, textBtn_ok2, pro::interfaceUI::KEYMODE::RELEASED };			// ������ ��
PROButton btn_setting		= { "setting",	{210,470, 300,470 , 300, 500, 210, 500}, textBtn_setting1, textBtn_setting2, pro::interfaceUI::KEYMODE::RELEASED };	// ������ ���������


// =================================== ���� ������ � ������ ==============================



Salut_info info{ 0,0 };			// ���������� � ������

//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			pro4types.h 
// ����������:		�������� ������� ������������ ������� ������� RGB � RGBA
//=============================================================================



#ifndef PRO4TYPES_H
#define PRO4TYPES_H



namespace pro {

	namespace types
	{

				

		class COLOR_RGBA
		{
		private:
			float m_r;
			float m_g;
			float m_b;
			float m_a;
		public:

			COLOR_RGBA();

			COLOR_RGBA(float t_r, float t_g, float t_b, float t_a);
			
			COLOR_RGBA(const COLOR_RGBA& other_color);
			
			COLOR_RGBA& operator=(const COLOR_RGBA& other_color);
			
			bool operator==(const COLOR_RGBA& other_color);
						
			void setColor();

			void clear();

		};



		class COLOR_RGB
		{
		private:
			float m_r;
			float m_g;
			float m_b;

		public:
			COLOR_RGB();

			COLOR_RGB(float t_r, float t_g, float t_b);

			COLOR_RGB(const COLOR_RGB& other_color);
			
			COLOR_RGB& operator=(const COLOR_RGB& other_color);
							
			bool operator==(const COLOR_RGB& other_color);
						
			void setColor();
			
			void clear(); 

		};


		
		
	}
}

#endif
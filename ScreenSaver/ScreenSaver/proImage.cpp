#include "proImage.h"


using namespace pro::filesystem;

// ��������� ������ �����������
int IMAGE::imageSize()
{
	int size = width * height;
	switch (bpp)
	{
	case 1: {size = size / 8; break; }
	case 2: {size = size / 4; break; }
	case 4: {size = size / 2; break; }
	default: {size = (size * bpp) / 8; break; }
	};
	return  (size < 0) ? (-size) : size;

}


//������������ ������
void IMAGE::clear()
{
	if (inddata) { delete[] inddata; inddata = nullptr; }
	if (pal) { delete[] pal; pal = nullptr; }
}



void IMAGE::init(int t_width, int t_height, int t_bpp, int t_sizepal, int t_bpal)
{
	width = t_width;
	height = t_height;
	bpp = t_bpp;
	IMAGE::clear();
	inddata = new uchar[imageSize()];
	if (sizepal)
		pal = new uchar[t_bpal / 8];
	


}

void IMAGE::fload_data(uchar* dst, int ic, int ia, int max, int order, FILE* f)
{
	for (int i = 0; i < max; i++)
	{
		// ������ ������� (rgba)
		fread(dst, 1, ic, f);
		// ���� �������� �������(argb) ��� bgra
		if (order == 1 || order == 2) { std::reverse(dst, dst + ic); }
		// ���� bgra
		if (order == 2) {std::rotate(dst, dst + 1, dst + ic);}
		dst = dst + ia;
	}

}

void IMAGE::fsave_data(uchar* src, int ic, int ia, int max, int order, FILE* f)
{
	uchar* temp = new uchar[ic];
	for (int i = 0; i < max; i++)
	{
		std::copy(src, src + ic, temp);
		// ���� �������� �������(argb) ��� bgra
		if (order == 1 || order == 2) { std::reverse(temp, temp + ic); }
		// ���� bgra
		if (order == 2) { std::rotate(temp, temp + 1, temp + ic); }
		fwrite(temp, 1, ic, f);	//���������� ������
		src = src + ia;
	}
	delete[] temp;
}

int IMAGE::loadbmp(const char* filename)
{
	header_bmp header;
	int size;			// ������ ������ ����������� � ������
	FILE* f = fopen(filename, "rb+");
	if (!f) return 0;
	clear();
	fread(&header, 1, 54, f);

	width = header.width;
	height = header.height;
	bpp = header.bpp;
	sizepal = header.color;

	// ��������� �������
	if (sizepal)
	{
		bpppal = 4;
		pal = new uchar[sizepal * 4];
		fseek(f, header.img_data - (sizepal * 4), SEEK_SET);
		fload_data(pal, 4, 4, sizepal, 2, f);
	}
	
	size = imageSize();
	inddata = new uchar[size];
	fseek(f, header.img_data, SEEK_SET);
	switch (bpp)
	{
	case 32: {fload_data(inddata, 4, 4, size / 4, 2, f); break; }
	case 24: {fload_data(inddata, 3, 3, size / 3, 1, f); break; }
	default: {fread(inddata, 1,  size, f); break; }
	}
	fclose(f);
	return 1;
}

int IMAGE::loadtga(const char* filename)
{
	header_tga header;
	int size;
	int retvalue = 1;
	FILE* f = fopen(filename, "rb+");
	if (!f) return 0;
	clear();
	// ��������� ���������
	fread(&header, 1, 18, f);
	if (header.idlen)
	{
		fseek(f, header.idlen, SEEK_CUR);
	}

	// ������������� �������� ������������ �����������
	width = header.width;
	height = header.height;
	bpp = header.bpp;
	// �������� �������
	if (header.typepal)
	{
		sizepal = header.colornum;
		bpppal = header.colorbpp;
		// � ��� ������ 32������ �������
		pal = new uchar[sizepal * 4];
		fload_data(pal, bpppal / 8, 4, sizepal, (bpppal == 24) ? 1 : 2, f);
	}
	else sizepal = 0;
	// ��������� �����������
	size = imageSize();
	inddata = new uchar[size];
	switch (bpp)
	{
	case 32: {fload_data(inddata, 4, 4, size / 4, 2, f); break; }
	case 24: {fload_data(inddata, 3, 3, size / 3, 1, f); break; }
	case 8: {fread(inddata, 1, size, f); break; }
	default: {retvalue = 0; break; }
		 
	}


	fclose(f);
	if (!retvalue) { clear(); return 0; }
	return 1;
}



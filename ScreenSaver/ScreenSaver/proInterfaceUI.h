//=============================================================================
// 
// ����������: ����� ������� 
// ver:	0.15 �� 30.12.2020
//
// ����:			proInterface.h 
// ����������:		�������� ��������� ���������� - ������
//					
//					
//					
//=============================================================================

#ifndef  INTERFACEUI_H
#define  INTERFACEUI_H





#include <string>


namespace pro {
	namespace interfaceUI
	{

		enum KEYMODE {RELEASED, MOVEDOWN, MOVEUP, PRESSED};


		// ��������� ��������� ������
		struct PROButton 
		{
			std::string name;
			float vertex_bt[8];				// ���������� �������������� ������� � �������� ������ ���� � �� ������� �������

			float* pTextureButtonON;		// �������� �� ������, ����� ������ ������
			float* pTextureButtonOFF;		// �������� �� ������, ����� ������ ��������

			KEYMODE key_state;				// ��������� ������

		};


		// ����������� ������ �� ������
		void PROButton_Show(PROButton button);


		// �������� ������� �� ������
		bool ClickOnButtonRect(int x, int y, PROButton button);
		


	}

}



#endif // ! INTERFACEUI_H
